DROP TABLE IF EXISTS regeln;
CREATE TABLE regeln (
    id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    regel varchar(64),
    wert int(4),
    PRIMARY KEY (id)
);

INSERT INTO regeln (`regel`,`wert`)
VALUES
('max_ausleihdauer',4),
('gebuehr_pro_woche',10),
('max_saldo',50),
('max_verlaengerung',4);