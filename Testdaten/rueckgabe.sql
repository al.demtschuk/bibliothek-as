DROP TABLE IF EXISTS rueckgabe;
CREATE TABLE rueckgabe (
    id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    ausleihe_id int(11) UNSIGNED NOT NULL,
    rueckgabe_soll date, -- Erstellung bei Ausleihvorgang
    rueckgabe_ist date, -- Erstellung bei Rückgabevorgang
    PRIMARY KEY (id),
    CONSTRAINT fk_rueckgabe_ausleihe FOREIGN KEY (ausleihe_id) REFERENCES ausleihe(id) ON UPDATE CASCADE
);

INSERT INTO rueckgabe (`ausleihe_id`,`rueckgabe_soll`,`rueckgabe_ist`)
VALUES
(1,'2021-10-08','2021-10-08'),
(2,'2021-10-16','2021-10-16'),
(3,'2021-10-24','2021-10-24'),
(4,'2021-11-01','2021-11-01'),
(5,'2021-10-12','2021-10-12'),
(6,'2021-10-20','2021-10-20'),
(7,'2021-10-28','2021-10-28'),
(8,'2021-11-05','2021-11-05'),
(9,'2021-10-16','2021-10-16'),
(10,'2021-10-24','2021-10-24'),
(11,'2021-11-01','2021-11-01'),
(12,'2021-11-09',NULL),
(13,'2021-10-20','2021-10-20'),
(14,'2021-10-28','2021-10-28'),
(15,'2021-11-05','2021-11-05'),
(16,'2021-11-13',NULL),
(17,'2021-10-24','2021-10-24'),
(18,'2021-11-01','2021-11-01'),
(19,'2021-11-09',NULL),
(20,'2021-11-17',NULL),
(21,'2021-10-28','2021-10-28'),
(22,'2021-11-05','2021-11-05'),
(23,'2021-11-13',NULL),
(24,'2021-11-21',NULL),
(25,'2021-11-01','2021-11-01'),
(26,'2021-11-09',NULL),
(27,'2021-11-17',NULL),
(28,'2021-11-25',NULL),
(29,'2021-11-05','2021-11-05'),
(30,'2021-11-13',NULL),
(31,'2021-11-21',NULL),
(32,'2021-11-29',NULL),
(33,'2021-11-09',NULL),
(34,'2021-11-17',NULL),
(35,'2021-11-25',NULL),
(36,'2021-12-03','2021-12-03'),
(37,'2021-10-08','2021-10-08'),
(38,'2021-10-16','2021-10-16'),
(39,'2021-10-24','2021-10-24'),
(40,'2021-11-01','2021-11-01'),
(41,'2021-10-12','2021-10-12'),
(42,'2021-10-20','2021-10-20'),
(43,'2021-10-28','2021-10-28'),
(44,'2021-11-05','2021-11-05'),
(45,'2021-10-16','2021-10-16'),
(46,'2021-10-24','2021-10-24'),
(47,'2021-11-01',NULL),
(48,'2021-11-09',NULL),
(49,'2021-10-20','2021-10-20'),
(50,'2021-10-28','2021-10-28'),
(51,'2021-11-05','2021-11-05'),
(52,'2021-11-13',NULL),
(53,'2021-10-24','2021-10-24'),
(54,'2021-11-01','2021-11-01'),
(55,'2021-11-09',NULL),
(56,'2021-11-17',NULL),
(57,'2021-10-28','2021-10-28'),
(58,'2021-11-05',NULL),
(59,'2021-11-13',NULL);