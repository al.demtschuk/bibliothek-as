DROP TABLE IF EXISTS verlaengerung;
CREATE TABLE verlaengerung (
    id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
    ausleihe_id int(11) UNSIGNED NOT NULL,
    anzahl_verlaengerung int(1),
    PRIMARY KEY (id),
    CONSTRAINT fk_verlaengerung_ausleihe FOREIGN KEY (ausleihe_id) REFERENCES ausleihe(id) ON UPDATE CASCADE
);

INSERT INTO verlaengerung (`ausleihe_id`,`anzahl_verlaengerung`)
VALUES
(1,1),
(2,2),
(3,3),
(4,4),
(5,1),
(6,2),
(7,3),
(8,4),
(9,1),
(10,2),
(11,3),
(12,4),
(13,1),
(14,2),
(15,3),
(16,4),
(17,1),
(18,2),
(19,3),
(20,4),
(21,1);